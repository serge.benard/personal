@extends('layouts.base')

{{-- @section('page-title', "") --}}
@section('page-header')
	@pagetitle
		Hi, I'm Serge B&eacute;nard!
	@endpagetitle

	@pagedescription
		Let's create something amazing together. The best part is you get to take all the credit.
	@endpagedescription

	@pagekeywords
		serge, benard, html, javascript, ottawa, gatineau, outaouais, hull, aylmer, ontario, quebec, management, admin, project, market, marketing, social, twitter, facebook, linkedin, linked in, ad, online, advertising, words, creative, technology, plotter, vinyl, cutter, photoshop, gimp, illustrator, microsoft, ubuntu, linux, programming, design, graphic, pro bono, free, gratis, gratuit, pdf, portable, graphic, file, svg, vector, jpg, png, gif, jpeg, format, scalable, tech
	@endpagekeywords
@endsection

@section('page-content')
	@include( 'partials.svgsprites' )
	{{-- @include('layouts.messages') --}}

	<div class="relative bg-monochrome-lightest">

		<div class="contact-box-holder">
			
			<div class="contact-box">
				<div class="contact-box-column column-one">
					<a href="mailto://serge.benard@gmail.com?subject={{ htmlentities("I'm writing from the website.") }}&body={{ htmlentities("I was visiting your website and I thought I would write you about ") }}">
						<svg><use href="#icon-envelope" /></svg>
						serge.benard@gmail.com
					</a>
	
					<a href="sms://{{ htmlentities("6132034976") }}">
						<svg><use href="#icon-telephone" /></svg>
						613-203-4976
					</a>
					
				</div>
				<div class="contact-box-column column-two">
					<a href="https://twitter.com/serge_benard" target="twitter">
						<svg><use href="#icon-twitter" /></svg>
						@serge_benard
					</a>
	
					<a href="https://www.instagram.com/sergebenard/" target="instagram">
						<svg><use href="#icon-instagram" /></svg>
						@sergebenard
					</a>
					
				</div>
	
			</div>
		</div>

		<div class="main-box-textbox">
			<h1 class="main-box-main-text">
				Decals
			</h1>
			<div class="main-box-secondary-text-holder">
				<h2 class="main-box-secondary-text-item">Buildings</h2>
				<h2 class="main-box-secondary-text-item">Vehicles</h2>
				<h2 class="main-box-secondary-text-item">Other Things</h2>
			</div>
		</div>

		<div class="main-box-asphalt"></div>

		<div class="main-box">

			<div class="icon-holder icon-appartment-building">
				<icon icon-id="icon-appartment-building"></icon>
			</div>

			<div class="icon-holder icon-shop">
				<icon icon-id="icon-shop"></icon>
			</div>

			<div class="icon-holder icon-commercial-truck">
				<icon icon-id="icon-commercial-truck"></icon>
			</div>

			<div class="icon-holder icon-pickup-truck">
				<icon icon-id="icon-pickup-truck"></icon>
			</div>

			<div class="icon-holder icon-car">
				<icon icon-id="icon-car"></icon>
			</div>
			
		</div>
	</div>

	<section class="page-section container">

		<a name="question-quote" id="question-quote" class="section-anchor"></a>

		<h2 class="page-subtitle">
			Question? Quote?
		</h2>
		@if( session('success'))
		<p class="text-monochrome-xl">
			Thank you for getting in touch!
		</p>

		<p>
			I usually reply within 24 hours of first contact, even on weekends!
		</p>
		@else
		<p>
			Often, the production of decals and lettering leaves most people unsure of the process and the costs involved.
		</p>
		<p class="text-monochrome-xl">
			Let's figure it out together.
		</p>

		<p>
			Just so you know, the more information I get from you at this point, the more answers I'll have when I contact you back.
		</p>
		<form-steps>
			<form-step step-name="start" step="1">
				<template slot="form-description-text">First, how can I help?</template>
				<template slot="form-content">
					<div class="form-element">
						<span class="inline-block">Do you want to:</span> <form-step-link next-step="question">send me a question or comment</form-step-link><span> or </span><form-step-link next-step="quote">request a quote?</form-step-link>
					</div>
				</template>
			</form-step>

			<form-step step-name="question" step="2">
				<template slot="form-description-text">Ask away, I can take it!</template>
				<template slot="form-content">
				
					<form role="form" method="POST" action="{{ route('contactSubmit') }}">
						@csrf
						<div class="form-element">
							<label class="form-label" for="formName">What's your name?</label>
							<input v-model="formName" type="text" class="form-input-text" name="formName" id="formName" required>
							<span class="form-element-helper">
								I'd like to know who you are.
							</span>
						</div>
						
						<div :class="{ 'text-red': ( formContactMethod !== '' && (!formContactIsEmail && !formContactIsPhone)) }" class="form-element">
							<label class="form-label" for="formContactMethod">Type in your email or phone number.</label>
							<input v-model="formContactMethod" @keyup="onFormContactMethodChange" type="text" class="form-input-text" :class="{ error: ( formContactMethod !== '' && (!formContactIsEmail && !formContactIsPhone)) }" name="formContactMethod" id="formContactMethod" required>
							
							<div class="block -ml-1 mt-2 sm:inline-block sm:mt-0 sm:ml-1" aria-live="polite" aria-atomic="true" v-if="formContactIsEmail">
								<input v-model="formSendCopy" type="checkbox" value="true" name="formSendCopy" id="formSendCopy" class="form-checkbox">
								<label class="form-checkbox-label" for="formSendCopy" clicked> <!-- form-checkbox-label -->
									CC this message to you?
								</label>
							</div>

							<div class="block mt-1">
								<span :class="{ error: ( formContactMethod !== '' && (!formContactIsEmail && !formContactIsPhone)) }" class="form-element-helper">
									I'd like an email or phone number so I can get back to you!
								</span>
							</div>
						</div>
						
						<div class="form-element">
							<label class="form-label" for="formQuestionTextarea">Please write your question or comment in the text box, and press the button below to send it to me.</label>
							<textarea v-model="formQuestionTextarea" class="form-element-textarea" name="formQuestionTextarea" id="formQuestionTextarea"></textarea>
							<span class="form-element-helper">
								<span class="font-semibold">
									You want help on what type of information I need to start a project?
								</span>
								It helps me to know the type of finish you're looking for - gloss, matte, etc. Do you need transparent, semi-transparent or perforated film? Is this film going on glass, or another surface? Is the film going to be exposed to the elements like sunlight, rain or snow?
							</span>
						</div>
						<div class="form-element submit-buttons-holder">
							<button class="button form-submit" type="submit">Send your message</button>
							<form-step-back>Back</form-step-back>
						</div>
					</form>
				</template>
			</form-step>

			<form-step step-name="quote" step="2">
				<template slot="form-description-text">All right, let's talk about your project.</template>
				<template slot="form-content">
					<p class="text-monochrome-xl">Please answer as much as you can.</p>
					<form role="form" method="POST" action="{{ route('quoteSubmit') }}">
						@csrf
						<div class="form-element">
							<label class="form-label" for="formName">What's your name?</label>
							<input v-model="formName" type="text" class="form-input-text" name="formName" id="formName" required>
							<span class="form-element-helper">
								I'd like to know who you are.
							</span>
						</div>
						
						<div :class="{ 'text-red': ( formContactMethod !== '' && (!formContactIsEmail && !formContactIsPhone)) }" class="form-element">
							<label class="form-label" for="formContactMethod">Type in your email or phone number.</label>
							<input v-model="formContactMethod" @keyup="onFormContactMethodChange" type="text" class="form-input-text" :class="{ error: ( formContactMethod !== '' && (!formContactIsEmail && !formContactIsPhone)) }" name="formContactMethod" id="formContactMethod" required>

							<div class="block -ml-1 mt-2 sm:inline-block sm:mt-0 sm:ml-1" aria-live="polite" aria-atomic="true" v-if="formContactIsEmail">
								<input v-model="formSendCopy" type="checkbox" value="true" name="formSendCopy" id="formSendCopy" class="form-checkbox">
								<label class="form-checkbox-label" for="formSendCopy" clicked> <!-- form-checkbox-label -->
									CC this message to you?
								</label>
							</div>

							<div class="block mt-1">
								<span :class="{ error: ( formContactMethod !== '' && (!formContactIsEmail && !formContactIsPhone)) }" class="form-element-helper">
									I'd like an email or phone number so I can get back to you!
								</span>
							</div>
						</div>

						<div class="form-element">
							<div class="flex items-center flex-wrap"> {{-- form-checkbox-group --}}
								<span class="inline-block whitespace-no-wrap">Do you want film</span>
								<input v-model="formApplicationArea" value="Inside a Building" type="checkbox" name="formApplicationArea[]" id="formCheckBoxInsideBuilding" class="form-checkbox">
								<label class="form-checkbox-label" for="formCheckBoxInsideBuilding"> <!-- form-checkbox-label -->
									inside a building,
								</label>

								<input v-model="formApplicationArea" value="Outside a Building" type="checkbox" name="formApplicationArea[]" id="formCheckBoxOutsideBuilding" class="form-checkbox">
								<label class="form-checkbox-label" for="formCheckBoxOutsideBuilding"> <!-- form-checkbox-label -->
									outside a building,
								</label>
								and/or
								<input v-model="formApplicationArea" value="Outside a Vehicle" type="checkbox" name="formApplicationArea[]" id="formCheckBoxOutsideVehicle" class="form-checkbox">
								<label class="form-checkbox-label" for="formCheckBoxOutsideVehicle"> <!-- form-checkbox-label -->
									outside a vehicle?
								</label>
							</div>

							<span class="block text-sm text-monochrome secondary-font">
								Select one or more.
							</span>
						</div>
						
						<div class="form-element">
							<div role="radiogroup" class="form-checkbox-group flex-wrap">
								<span class="inline-block whitespace-no-wrap">Do you know how much film you need?</span>
								<input v-model="formFilmSize" value="1" type="radio" name="formFilmSize" id="formRadioFilmSizeYes" class="form-checkbox">
								<label class="form-checkbox-label" for="formRadioFilmSizeYes" @click="formFilmAmount = true">
									Yes!
								</label>
								<input v-model="formFilmSize" value="0" type="radio" name="formFilmSize" id="formRadioFilmSizeNo" class="form-checkbox">
								<label class="form-checkbox-label" for="formRadioFilmSizeNo" @click="formFilmAmount = false">
									No.
								</label>
							</div>
						</div>
						<div class="form-element" aria-live="polite" aria-atomic="true" v-if="formFilmAmount">
							<div class="form-film-amount">
								<label for="formFilmAmount" class="inline-block whitespace-no-wrap">How much film?</label>
								<input v-model="formFilmAmount" id="formFilmAmount" type="number" min="1" max="10000" step="0.25" size="6" class="form-input-text ml-1" name="formFilmAmount">
								
								<input v-model="formFilmMeasurement" type="radio" name="formFilmMeasurement" value="Square Feet" id="formRadioFilmSizeFeet" class="form-checkbox">
								<label class="form-film-measure" for="formRadioFilmSizeFeet">
									Ft&sup2;
								</label>
								<input v-model="formFilmMeasurement" type="radio" name="formFilmMeasurement" value="Square Meters" id="formRadioFilmSizeMeter" class="form-checkbox">
								<label class="form-film-measure" for="formRadioFilmSizeMeter">
									M&sup2;
								</label>
							</div>
						</div>

						<div class="form-element">
							<label class="form-label" for="formQuestionTextarea">Please write your question or comment in the text box, and press the button below to send it to me.</label>
							<textarea v-model="formQuestionTextarea" class="form-element-textarea" name="formQuestionTextarea" id="formQuestionTextarea"></textarea>
							<span class="form-element-helper">
								<span class="font-semibold">
									You want help on what type of information I need to start a project?
								</span>
								It helps me to know the type of finish you're looking for - gloss, matte, etc. Do you need transparent, semi-transparent or perforated film? Is this film going on glass, or another surface? Is the film going to be exposed to the elements like sunlight, rain or snow?
							</span>
						</div>

						<div class="form-element submit-buttons-holder">

							<button class="button form-submit" type="submit">Send your message</button>

							<form-step-back>Back</form-step-back>
						</div>
					</form>
				</template>
			</form-step>
			<a class="button form-back" href="#contact">
				I just want to talk with someone.
			</a>
		</form-steps>

		@endif

	</section>

	<section class="page-section container">
		<a name="contact" id="contact" class="section-anchor"></a>

		<h2 class="page-subtitle">Contact Me</h2>

		<p>The two best ways of getting in touch with me <a href="mailto://serge.benard@gmail.com?subject={{ htmlentities("I'm writing from the website.") }}&body={{ htmlentities("I was visiting your website and I thought I would write you about ") }}">is by email</a>, and <a href="sms://{{ htmlentities("6132034976") }}">text message to my mobile</a>.</p>

		<p>Whichever method you choose, you can be sure I will do everything I can to get back to you within 24 hours of the initial contact - even on weekeneds.</p>
	</section>

	<section name="About Me" class="page-section container">
		<a name="about-me" id="about-me" class="section-anchor"></a>

		<h2 class="page-subtitle">About Me</h2>

		<p>I started helping my father at his job when I was around 13 years old. Since then, I have had many different occupations in my life.</p>

		<p>One thing I learned about work pretty early on is that I need to like what I do for work for me to be happy. I'm sure this is true for most people. As it turns out, when I work at something I enjoy, I do much better quality work. I'm sure I'm not the only person for whom this applies.</p>

		<p>Four years ago, I had an opportunity to work in the Vinyl Film industry. I was planning, designing, and producing vinyl film signs, decals, all types of things. I noticed I like doing this... <span class="inline-block font-bold text-sm">I like doing this a lot.</span></p>

		<p>While working with vinyl film, I can be creative when planning and designing; I can also be a problem solver and find solutions for people's needs; I get to work with technology! I <span class="inline-block font-bold text-sm">love</span> technology!</p>

		<p>I come from a big family - <span class="inline-block font-bold text-sm">we are seven kids!</span> My parents worked hard to make sure we were all growing up into happy and healthy adults. I learned many things from my parents, but two things in particular have really stuck with me - a strong connection to family, and a work ethic which is just as strong as that family bond.</p>

		<p>My goal in life is to be the best person I can be. Since I love what I do for work <span class="inline-block font-bold text-sm">so much</span>, I feel the connections I make with the people I meet professionally are more genuine, and are worth more to me than if I was working at <span class="inline-block font-bold text-sm">Just A Job</span>.</p>

		<p><span class="inline-block text-lg font-bold text-monochrome">This is my promise to you:</span> I will work very, very hard to make sure I provide the best solution I possibly can to meet your needs, any and every time you come to me with a question.</p>

		<p>So <a href="#contact">why don't you get in touch with me</a>. Let's talk, and if you feel we get along, we'll get to make amazing things together.</p>

		<p class="text-monochrome-xl">The great thing is, you can tell others it was all you.</p>
	</section>

	<footer class="mt-6 border-t-2 border-monochrome bg-monochrome-lightest text-center py-6">
		<div class="mx-auto text-monochrome-light">
			All content Copyright &copy; {{ date('Y') }}, Serge B&eacute;nard
		</div>
	</footer>
@endsection


@section('page-footer-script')
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
<script>
	window.Event = new Vue();

	Vue.component('form-step-link', {
		template: `<a href="javascript:void(0)" @click="updateStep( $event, nextStep )"
						role="button"
						class="button form-step"><slot></slot></a>`,
		methods: {
			updateStep( ) {
				console.log( 'updateStep nextStep: ' + this.nextStep )
				Event.$emit('next-step', this.nextStep);
			}
		},
		props: [
			'next-step'
		]
	});

	Vue.component('form-step-back', {
		template: `<a href="javascript:void(0)" @click="updateStep( 'start' )"
						role="button"
						class="button form-back"><slot></slot></a>`,
		methods: {
			updateStep( step ) {
				console.log( 'updateStep nextStep: ' + step )
				Event.$emit('next-step', step);
			}
		}
	});

	Vue.component('form-steps', {
		data() {
			return {
				currentStep: 'start'
			}
			
		},
		template: `
		<div class="steps-holder">
			<slot></slot>
		</div>
		`,
		methods: {
			selectStep( step ) {
				console.log('Inside selectStep. ', step);
				this.currentStep = step;
			}
		},
		created() {
			let vm=this;
			Event.$on('next-step', function( step ) {
				vm.selectStep( step )
			})
			// this.stepsObjects = this.$children;
			// console.log(this.$children);
		}
	});

	Vue.component('form-step', {
		// <div v-if="this.$parent.currentStep === stepName">
		template: `
		<div class="step-holder" aria-live="polite" aria-atomic="true" v-if="this.$parent.currentStep === stepName">
			<div class="step-description-holder">
				<p class="step-description mr-2">
					<slot name="form-description-text"></slot>
				</p>
				<span class="secondary-font text-monochrome-light text-lg whitespace-no-wrap">
					Step @{{ step }} of 2
				</span>
			</div>

			<div class="block">
				<slot name="form-content"></slot>
			</div>
		</div>`,
		props: [
			'step',
			'step-name'
		],
	});

	Vue.component('icon', {
		template: `<img @toggleBuildings="toggle = !toggle" @mouseover="toggle = true" @mouseleave="toggle = false" :src="'{{ asset('/images/front-page/') }}/' + iconId + ( toggle ? '-full' : '-mono' ) + '.png'" class="main-icon" :class="iconId">`,
		data: function() {
			return {
				toggle: false,
			}
		},
		props: [
			'icon-id'
		]
	})

	var app = new Vue({
		el: '#app',
		data: { /**
				* All form elements:
				* Quote form: formName[string], formContactMethod[string, digits], formSendCopy[bool], formApplicationArea[array],
				* 		formFilmSize[bool], formFilmAmount[digit], formFilmMeasurement[string], formQuestionTextarea[string]
				*/
			//formFilmAmount: true, // Show or hide film amount field and label
			formContactIsEmail: false,
			formContactIsPhone: false,
			formName: '',
			formContactMethod: '',
			formQuestionTextarea: '',
			formSendCopy: 'true',
			formApplicationArea: [],
			formFilmSize: '1',
			formFilmAmount: 'true',
			formFilmMeasurement: 'Square Feet',
			


		},
		methods: {
			onFormContactMethodChange( event ) {
				this.formContactIsEmail = this.checkIfEmail( this.formContactMethod );
				this.formContactIsPhone = this.checkIfPhone( this.formContactMethod );
			},
			checkIfEmail( email ) {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				
				return re.test(String( email ).toLowerCase());
				
			},
			checkIfPhone( phone ) {
				var re = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ex?t\.?|extension)\s*(\d+))?$/;
				// /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension|x)\s*(\d+))?$^;

				return re.test(String( phone ).toLowerCase());

			}
		}
	});
</script>
@endsection