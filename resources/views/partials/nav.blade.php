	<header class="page-header bg-monochrome-lightest">
		<!-- Main Navigation -->
		<nav role="navigation" class="main-navigation-holder">
			<ul class="main-navigation">
				<!-- Logo -->
				<li class="navigation-badge-logo">
					<img class="" src="{{ asset('/images/sb-logo.png') }}" alt="Serge Benard Logo">
				</li>
				<li class="navigation-badge-text-holder">
					<p class="logo-text-normal">
						Serge
					</p>
					<p class="logo-text-medium">
						B&eacute;nard
					</p>
				</li>
				<li class="menu-holder">
					<ul class="menu">
						<li class="menu-item">
							<a href="{{ url('/') }}#home">
								Home
							</a>
						</li>
						<li class="menu-item">
							<a href="{{ url('/') }}#question-quote">
								Question/Quote
							</a>
						</li>
						<li class="menu-item">
							<a href="{{ url('/') }}#about-me">
								About
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</header>