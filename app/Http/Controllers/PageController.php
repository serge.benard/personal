<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mail\Contact;
use App\Mail\Quote;

use App\Rules\PhoneOrEmail;

class PageController extends Controller
{
	public function welcome() {
		// dump( is_writable(config('session.files')) );
		return view('welcome');
	}

	public function contactSubmit(Request $request) {

		/**
		 * All form elements:
		 * Contact form: formName[string], formContactMethod[string, digits], formSendCopy[bool], formQuestionTextarea[string]
		 * Quote form: formName[string], formContactMethod[string, digits], formSendCopy[bool], formApplicationArea[array],
		 * 		applicationFilmSize[bool], formFilmAmount[digit], formFilmMeasurement[string], formQuestionTextarea[string]
		 */
		$request->validate([
			'formName' => 'nullable|max:250',
			'formContactMethod' => ['required', new PhoneOrEmail ],
			'formQuestionTextarea' => 'nullable'
		]);

		$data = [	'formName' =>				( !empty( $request->formName ) ? $request->formName : 'Website Visitor' ),
					'formEmail' =>				( $this->isEmail( $request->formContactMethod ) ? $request->formContactMethod : false),
					'formPhone' =>				( $this->isPhone( $request->formContactMethod ) ? $request->formContactMethod : false),
					'formQuestionTextarea' => 	$request->formQuestionTextarea
		];

		// return new Contact( $data );

		$mail = Mail::to( 'serge@sergebenard.com', 'Serge Benard' );

		if ( $request->formSendCopy && $data['formEmail'] !== false ) {
			$mail->cc( $data['formEmail'], $data['formName']);
		}

		$mail->send( new Contact( $data ) );

		Session::flash('success', 'Your message has been sent. I\'ll get in touch with you shortly!');

		return redirect( route( 'welcome' ) . '#question-quote' );
	}

	public function quoteSubmit(Request $request) {

		/**
		 * All form elements:
		 * Quote form: formName[string], formContactMethod[string, digits], formSendCopy[bool], formApplicationArea[array],
		 * 		applicationFilmSize[bool], formFilmAmount[digit], formFilmMeasurement[string], formQuestionTextarea[string]
		 */
		$request->validate([
			'formName' => 'nullable|max:250',
			'formContactMethod' => ['required', new PhoneOrEmail ],
			'formQuestionTextarea' => 'nullable'
		]);

		$data = [	'formName' =>				( !empty( $request->formName ) ? $request->formName : 'Website Visitor' ),
					'formEmail' =>				( $this->isEmail( $request->formContactMethod ) ? $request->formContactMethod : false),
					'formPhone' =>				( $this->isPhone( $request->formContactMethod ) ? $request->formContactMethod : false),
					'formApplicationArea' =>	( ( isset( $request->formApplicationArea ) && count( $request->formApplicationArea ) >= 1 ) ? $request->formApplicationArea : false),
					'formFilmAmount' =>			( ( isset( $request->formFilmAmount ) && $request->formFilmAmount != '' ) ? $request->formFilmAmount : false ),
					'formFilmMeasurement' =>	( ( isset( $request->formFilmMeasurement ) && $request->formFilmMeasurement != '' ) ? $request->formFilmMeasurement : false ),
					'formQuestionTextarea' => 	$request->formQuestionTextarea
		];
		// dd( $data['formApplicationArea'] );
		// return new Quote( $data );

		$mail = Mail::to( 'serge@sergebenard.com', 'Serge Benard' );

		if ( $request->formSendCopy && $data['formEmail'] !== false ) {
			$mail->cc( $data['formEmail'], $data['formName']);
		}

		$mail->send( new Quote( $data ) );

		Session::flash('success', 'Your message has been sent. I\'ll get in touch with you shortly!');

		return redirect( route( 'welcome' ) . '#question-quote' );
	}
	
	private function isEmail( $value ) {

		$emailReg = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';

		return preg_match( $emailReg, $value );

	}

	private function isPhone( $value ) {
		
		$phoneReg = '/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ex?t\.?|extension)\s*(\d+))?$/';

		return preg_match( $phoneReg, $value );
	}

}
