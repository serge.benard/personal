<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<style>
		@include( 'email.css' )
	</style>

	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Thank you for contacting me!</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway:300,400,700,900" rel="stylesheet">

	{{-- <link rel="stylesheet" href="{{ asset('/css/app.css') }}"> --}}

</head>
<body>
	
	<header class="email-header">
		<ul class="email-badge-holder">
			<!-- Logo -->
			<li class="email-badge-logo">
				<a href="{{ url('/') }}">
					<img src="{{ $message->embed( asset('/images/sb-logo.png') ) }}" alt="Serge Benard Logo">
				</a>
			</li>
			<li class="email-badge-text-holder">
				<p class="logo-text-normal">
					<a href="{{ url('/') }}">
						Serge
					</a>
				</p>
				<p class="logo-text-medium">
					<a href="{{ url('/') }}">
						B&eacute;nard
					</a>
				</p>
			</li>
		</ul>
	</header>

	<section name="EmailBody" class="email-body">
		<div class="email-page-content">
			@yield('page-content')
		</div>
	</section>
	
	@yield( 'page-footer' )
</body>
</html>