@extends('layouts.emailbase')
{{-- /**
* All form elements:
* Contact form: formName[string], formContactMethod[string, digits], formSendCopy[bool], formQuestionTextarea[string]
* Quote form: formName[string], formContactMethod[string, digits], formSendCopy[bool], formApplicationArea[array],
* 		applicationFilmSize[bool], formFilmAmount[digit], formFilmMeasurement[string], formQuestionTextarea[string]
*/ --}}

@section('page-content')
	
		<h1>A new email from {{ $data['formName'] }}</h1>
		
		<div class="email-message-holder">
			<div class="email-block">
				<span class="email-message-subtitle">
					From:
				</span>
				<p class="email-inline-block">
					{{ $data['formName'] }}
				</p>
			</div>
			@if( $data['formPhone'] )
			<div class="email-block">
				<span class="email-message-subtitle">
					Phone:
				</span>
				<a class="email-inline-block" href="tel:+{{ $data['formPhone'] }}">{{ $data['formPhone'] }}</a>
			</div>
			@endif

			<div class="email-block">
				<span class="email-message-subtitle align-top">Message:</span>
				<span class="email-inline-block">
					{!! nl2br( e( $data['formQuestionTextarea'] ) ) !!}
				</span>
			</div>
		</div>

		<div class="email-thanks">
			<h2>
				Thanks again for contacting me!
			</h2>
			<p>I usually reply within 24 hours of receiving the initial message - even on weekends!</p>
			<p>Just so you know, I believe in privacy. I don't share anyone's email address without their prior consent.</p>
		</div>

@endsection

@section('page-footer')
	<div class="email-footer">
		<p>
			<a href="tel:+1{{ __('global.contactPhoneRaw') }}">{{ __('global.contactPhoneFormatted') }}</a>
		</p>
		<p>
			<a href="{{ url('/') }}">sergebenard.com</a>
		</p>
	</div>
@endsection