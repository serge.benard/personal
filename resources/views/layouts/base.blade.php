<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
@yield('page-header')
	
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	{{-- <title>@yield('page-title', "Hello, I am Serge B&eacute;nard!")</title> --}}

	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway:300,400,700,900" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('/css/app.css') }}">

	<link rel="icon" href="{{ url('images/favicon.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('/images/apple-icon-57x57-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/images/apple-icon-72x72-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/images/apple-icon-114x114-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('/images/apple-icon-144x144-precomposed.png') }}">

</head>
<body>
	<!-- Navigation -->
	@include( 'partials.nav' )

	<!-- /Navigation -->
	
	<!-- Page Main -->
	<main id="app">
		@yield('page-content')
	</main>

	<!-- /Page Main -->

	<!-- Page Footer -->
	@yield( 'page-footer-script' )

	<!-- /Page Footer -->
</body>
</html>