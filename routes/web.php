<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('/contact', 'PageController@contactSubmit')
			->name('contactSubmit');
Route::post('/contactez', 'PageController@contactezSubmit')
			->name('contactezSubmit');

Route::post('/quote', 'PageController@quoteSubmit')
			->name('quoteSubmit');
Route::post('/citer', 'PageController@citerSubmit')
			->name('citerSubmit');

Route::get('/checkContactEmail', 'PageController@checkContactEmail' )
			->name('checkContactEmail');

Route::get('/checkQuoteEmail', 'PageController@checkQuoteEmail' )
			->name('checkQuoteEmail');
			
Route::get('/', 'PageController@welcome')->name('welcome');
