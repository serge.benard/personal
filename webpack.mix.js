let mix = require('laravel-mix')
require('laravel-mix-purgecss')

var tailwindcss = require('tailwindcss');

mix.postCss('resources/assets/css/app.css', 'public/css', [
		tailwindcss('./tailwind.js')
	])
	// .postCss('resources/assets/css/email.css', 'public/css',[
	// 	tailwindcss('./tailwind.js')
	// ])
	.purgeCss(
		
	)
	// .js('resources/assets/js/app.js', 'public/js')
